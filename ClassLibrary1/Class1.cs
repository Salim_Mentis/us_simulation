﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class RandomDateTime
    {
        DateTime start;
        DateTime end;
        Random gen;
        int range;

        //public void RandomDateT()
        //{
        //    start = new DateTime(2017, 1, 1);
        //    end = new DateTime(2017, 12, 31);
        //    gen = new Random();
        //    range = (end - start).Days;
        //}

        public DateTime Next()
        {
            start = new DateTime(2017, 1, 1);
            end = new DateTime(2017, 12, 31);
            gen = new Random();
            range = (end - start).Days;
            return start.AddDays(gen.Next(range));

        }
    }

    public class GetPercentage
    {
        int percentage = 0;

        public void GetPerc(int i, int randomsize, int distinctvalues)
        {
            percentage = Convert.ToInt32(i / Convert.ToDouble((randomsize / distinctvalues)) * 100);
            Console.Write(percentage + "% ");
        }

    }

    public class TimeLeft
    {

        int percentage = 0;

        public void TimeL(int i, int randomsize, int distinctvalues, DateTime start)
        {
            percentage = Convert.ToInt32(i / Convert.ToDouble((randomsize / distinctvalues)) * 100);
            TimeSpan ts = (DateTime.Now - start);
            if (percentage > 0)
            {
                ts = TimeSpan.FromTicks(ts.Ticks * (100 / percentage));
                string countDown = string.Format("--> {0} Hours, {1} Minutes, {2} Seconds until File is ready.", ts.Hours, ts.Minutes, ts.Seconds);
                Console.WriteLine(countDown);
                //timeleft = Convert.ToDouble(ts) * (100 / percentage);
            }

        }

    }
}
