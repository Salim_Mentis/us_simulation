﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary1;

namespace US_Simulation
{
    class Program
    {
        static void Main(string[] args)
        {
                int lowerRange = 1;
                int upperRange = 1110000;
                string filename = @"D:\Mars\Temp_file\test.txt";
                Random r = new Random();
                int number = 0;
                int randomsize = 68800000;
                int distinctvalues = 1100000;
                DateTime start = DateTime.Now;
            
                //Call funtion daterandom & timeleft             
                RandomDateTime Objdate = new RandomDateTime();
                TimeLeft objtimeleft = new TimeLeft();

                //Loops
                 using (StreamWriter writer = new StreamWriter(filename, false, Encoding.UTF8))
                 {
                
                    for (int i = 0; i <(randomsize / distinctvalues); i++)
                    {
                        if(i==0)
                        {
                            //Adding headers
                            writer.WriteLine("HouseholdId" + "," + "Date" + ",");
                        }
                        number = r.Next(lowerRange, upperRange);

                        //Display Percentage progress in console                       
                        Console.Clear();

                        GetPercentage oPercentage = new GetPercentage();
                        oPercentage.GetPerc(i, randomsize, distinctvalues);
                        objtimeleft.TimeL(i, randomsize, distinctvalues, start);
                        
                        //time remaining
                       // Console.WriteLine((start - DateTime.Now).ToString());

                    //write lines
                    for (int j = 0; j < distinctvalues; j++)
                         {
                            writer.WriteLine(number + "," + Objdate.Next().ToString("yyyy-MM-dd") + ",");  
                            if(i == ((randomsize / distinctvalues) -1) & j == (distinctvalues - 1))
                            {
                                writer.WriteLine(number + "," + Objdate.Next().ToString("yyyy-MM-dd"));
                            }
                         }     

                    }
                    writer.Flush();
                    writer.Close();
                 }
            
        }
    }

}
